comidas(cecina, morelos, res, plato_fuerte, carnes, centro_sur, agridulce, rojo, suave).
comidas(pipianverde, morelos, pollo, plato_fuerte, carnes, centro_sur, dulce, verde, suave).
comidas(caldo_de_hongos, morelos, hongos, plato_fuerte, vegetales, centro_sur, salado, gris, suave).
comidas(tacos_acorazados, morelos, res, plato_fuerte, carnes, centro_sur, salado, guinda, suave).
comidas(pozole_blanco, morelos, milaneza_de_bola, entrada, carne_vegetales, centro_sur, acido, rojo, liquido).
comidas(tamal_de_pescado, morelos, mojarra, plato_fuerte, vegetales, centro_sur, salado, cafe, esponjoso).
comidas(esquites, morelos, elote, dulce, vegetales, centro_sur, agridulce, amarillo, suave).
comidas(huauzontle, morelos, huazontle, entrada, vegetales, centro_sur, salado, rojo, liquido).
comidas(tacoyos, morelos, pure_de_frijoles, plato_fuerte, vegetales, centro_sur, agri_dulces, verde, masa).
comidas(itacates, morelos, flores_de_calabaza, entrada, vegetales, centro_sur, dulce, naranja, masa).
comidas(tacos_de_chapulines, morelos, chapulines, entrada, insectos, centro_sur, dulce, guinda, crujiente).
comidas(frijoles_ayocotes_en_adobo, morelos, frijoles_acoyotes, complemento, vegetales, centro_sur, dulce, morado, solido).
comidas(pescado_sarandeado, nayarit, huachinango, entrada, pescados, oeste, salado, cafe, dorado).
comidas(pollo_al_estilo_de_ixtlan_del_rio, nayarit, pollo_de_corral, plato_fuerte, carnes, oeste, agridulce, cafe, dorado).
comidas(pipian_de_pepitas_de_calabaza, nayarit, semillas_de_calabazas, plato_fuerte, especias, oeste, agridulce, naranja, semiliquido).
comidas(frijoles_puercos, nayarit, papitas_de_cerdo_en_vinagre, entrada, vegetales, oeste, salado, verde, suave).
comidas(tostadas_de_chanfaina, nayarit, patitas_de_cerdo_en_vinagre, entrada, pescados, oeste, salado, verde, suave).
comidas(sopa_de_ostion, nayarit, filete_de_pescado, plato_fuerte, pescados, oeste, salado, cafe, crujinete).
comidas(tostadas_de_chanfaina, nayarit, camaron, entrada, pescados, oeste, salado, cafe, dorado).
comidas(pescado_sarandeado, nayarit, huachinango, entrada, pescados, oeste, salado, cafe, dorado).
comidas(pollo_al_estilo_de_ixtla_del_rio, nayarit, pollo, plato_fuerte, carnes, oeste, agridulce, amarillo, asado).
comidas(sopa_de_ostion, nayarit, ostiones, plato_fuerte, pescados, oeste, salado, naranja, suave).
comidas(machaca, monterrey, carne_seca_de_res, plato_fuerte, carnes, norte, salado, rojo, seco).
comidas(cabrito_al_pastor, monterrey, cabrito_de_lechal, plato_fuerte, carnes, norte, dulce, rojo, asado).
comidas(capirotada, monterrey, piloncillo, postre, dulces, norte, dulce, cafe, horneado).
comidas(frijoles_con_veneno, monterrey, chile_carne, plato_fuerte, carnes, norte, salado, carmesi, suave).
comidas(cuajitos, monterrey, res, plato_fuerte, carnes, norte, agridulce, cafe, liquido).
comidas(machaca, monterrey, carne_seca_de_res, plato_fuerte, carnes, norte, salado, rojo, seco).
comidas(mole_negro, oaxaca, chile_negro, plato_fuerte, carnes, centro, agridulce, negro, semiliquido).
comidas(coloradito, oaxaca, chile_ancho, plato_fuerte, carnes_chile, centro, agridulce, cafe, semiliquido).
comidas(tasajo, oaxaca, tasajo, entrada, verduras, centro, amargo, verde, caldo).
comidas(caldo_de_piedra, oaxaca, piedra_filete_de_pescado, plato_fuerte, pescados, centro, salado, gris, caldo).
comidas(mole_poblano, puebla, pavo_chile, plato_fuerte, guiso, sur_central, agridulce, cafe, cremosa).
comidas(chile_en_nogada,puebla, pavo_chile, plato_fuerte, vegetales, sur_central, picante, cafe, suave).
comidas(cemitas_poblanas, puebla, pavo_chile, plato_fuerte, carnes, sur_central, dulce_picante, cafe, suave).
comidas(enchiladas_de_pipian, puebla, chile_serrano_pepita_calabaza, plato_fuerte, vegetariana, sur_central, dulce, amarillas, suaves).
comidas(molotes, puebla, chile_serrano, plato_fuerte, guiso, sur_central, picante, cafe, verde).
comidas(la_pasita, puebla, licor_de_pasas, bebida, licor, sur_central, dulce, morada, bedida).
comidas(tortias_de_santa_clara, puebla, manteca_de_cerdo, plato_fuerte, guiso, sur_central, salado, amarillo, suave).
comidas(camotes_poblanos, puebla, camotes, dulce, postre, sur_central, dulce, morado, suave).
comidas(carne_tatemada, aguascalientes, carne_de_puerco, plato_fuerte, carnes, occidente, picosa, cafe, suave).
comidas(pollo_san_marcos, aguascalientes, chile_serrano, plato_fuerte, carnes, occidente, picosa, rojo, dorada).
comidas(sopa_campesina, aguascalientes, elotitos_tiernos, plato_fuerte, vegetales, occidente, amarga, verde,duro).
comidas(chile_aguascalientes, aguascalientes, chile_ancho, plato_fuerte, vegetales, occidente, picosa, rojo, suave).
comidas(lechon_al_horno, aguascalientes, lechon, plato_fuerte, carnes, occidente, agridulce, cafe, asado).
comidas(langostaestilo_puerto_nuevo, baja_california, langosta, plato_fuerte, carnes, noroeste, picosa, naranja, frita).
comidas(tacos_de_pescado_a_la_tempura, baja_california, pescado_frito, plato_fuerte, carnes, noroeste, amarga, cafe, dorada).
comidas(baja_med, langosta, baja_california, plato_fuerte, carnes, noroeste, picosa, naranja, frita).
comidas(filete_imperial_de_camaron, baja_california_sur, camrones_cocidos, plato_fuerte, carnes, noroeste, salada, naranja, frita).
comidas(machaca_de_res, baja_california_sur, carne_seca_de_res, plato_fuerte, carnes, noroeste, salada, cafe, suave).
comidas(langosta, langosta_hervida, baja_california_sur, plato_fuerte, carnes, noroeste, salada, cafe, suave).
comidas(birria_estilo_bcs, baja_california_sur, carne_chivo, plato_fuerte, carnes, noroeste, salada, cafe, dorada).
comidas(machaca_mantarraya, baja_california_sur, machaca_deshidratada, plato_fuerte, carnes, noroeste, agridulce, cafe, suave).
comidas(camrones_rellenos, baja_california_sur, camrones, plato_fuerte, pescados, noroeste, salada, naranja, frita).
comidas(almejas_en_escabeche, baja_california_sur, camrones_sin_concha, plato_fuerte, pescados, noroeste, salada, gris, dura).
comidas(pan_de_cazon, campache, tortillas_sala_habanero, postre, carne_energeticos, sureste, dulce, gris, dura).
comidas(camarones_al_coco, campache, camarones_empanizados, plato_fuerte, carnes, sureste, agridulce, naranja, suave).
comidas(dulce_de_ciriote, campache, ciriotes, postre, energeticos, sureste, dulce, cafe, pan).
comidas(puchero, campache, res_cerdo, plato_fuerte, carne_energeticos, sureste, agridulce, ginda, termino_medio).
comidas(bistec_de_cazuela, campache, lomo_de_res, plato_fuerte, carnes, sureste, salado, cafe, suave).
comidas(chocolomo, campache, lomo_de_res, plato_fuerte, carnes, sureste, agridulce, amarillo, suave).
comidas(pampano_enscabeche, campache, chile, plato_fuerte, verduras, sureste, salado, verde, ensalada).
comidas(sopa_de_fiesta, chiapas, pan_colero, plato_fuerte, energeticos, sureste, dulce, cafe, horneado).
comidas(tamal_chiapaneco, chiapas, azafran, cena, energeticos, sureste, dulce, cafe, esponjoso).
comidas(tamal_bola, chiapas, chile_simojovel, cena, energeticos, sureste, dulce, cafe, esponjoso).
comidas(chochito_horneado, chiapas, hierbas_aromaticas, plato_fuerte, carnes, sureste, aamargo, cafe, suave).
comidas(tascalate, chiapas, cacao_en_polvo, plato_fuerte, carnes, sureste, dulce, cafe, cremoso).
comidas(queso_relleno_de_Ocosingo, chiapas, aceituna, plato_fuerte, lacteos, sureste, dulce, amarillo, cremoso).
comidas(mole_chiapaneco, chiapas, chile_ancho, plato_fuerte, carnes, energeticos, agridulce, rojo, semiliquido).
comidas(pan_de_pulque, coahuila, pulque, postre, energeticos, noreste, dulce, cafe, suave).
comidas(discada, coahuila, vegetales_mixtos, comida, carnes, noreste, agridulce, cafe, asada).
comidas(frijoles_puercos, colima, frijoles_manteca, comida, carnes, occidente, salada, cafe, suave).
comidas(sopitos, colima, tostada, entrada, carnes, occidnete, salada, verde, suave).
comidas(cuachala, colima, pechuga_de_pollo, comida, carnes, occidente, salada, amarilla, asada).
comidas(frijoles_refritos, colima, frijoles_pollo, comida, vegetales, occidente, amargo, naranja, suave).
comidas(tejuino, colima, atole_de_maiz, bebida, vegetales, occidente, dulce, amarillo, liquido).
comidas(frijoles_refritos, colima, frijoles_pollo, comida, vegetales, occidente, amargo, naranja, suave).
comidas(ponche_comala, colima, granada, bedida, fruta, occidente, dulce, naranja, liquido).
comidas(caldo_oso, chihuahua, trozos_de_pezcado, comida, carnes, noreste, salado, cafe, liqido).
comidas(caldo_oso, chihuahua, trozos_de_pezcado, comida, carnes, noreste, salado, cafe, liqido).
comidas(machaca, chihuahua, carne_seca_desebrada, comida, carnes, noreste, salado, cafe, asado).
comidas(sotol, chihuahua, cabeza_o_pina, bebida, frutas, noreste, amargo, amarillo, suave).
comidas(burritos, chihuahua, carne_desebrada, comida, carnes, noreste, salado, blanco, suave).
comidas(empanadas, chihuahua, harina_trigo, postre, carnes, noreste, agridulce, cafe, esponjoso).
comidas(quesadillas_sin_queso, distrito_federal, tortilla, entrada, carnes, centro, agridulce, amarilla, suave).
comidas(tacos, distrito_federal, tortilla, entrada_comida_cena, carnes, centro, agridulce_salado, amarilla, suave).
comidas(pancita, distrito_federal, estomago_de_res, entrada, carnes, centro, salado, rojo, suave).
comidas(caldo_de_gallina, distrito_federal, carne_gallina, entrada, carnes, centro, salado, naranja, suave).
comidas(flautas, distrito_federal, tortilla, cena, carnes, centro, dulce, verdes, suave).
comidas(tortas_de_manual, distrito_federal, tamal, cena, carnes, centro, dulce, amarilla, suave).
comidas(tlacoyos, distrito_federal, tortilla_gruesa, desyuno, energeticos, centro, agridulce, verdes, suave).
comidas(caldillo_duranguense, durango, carne_seca, comida, carnes, noreste, salada, roja, asada).
comidas(enchiladas_duranguenses, durango, cacahuates, comida, energeticos, noreste, agridulces, roja, suaves).
comidas(dulce_nuez, durango, nuez, postre, eneergeticos, noreste, dulces, cafes, suaves).
comidas(gallinas_borrachas, durango, carne_de_gallina, comida, carnes, noreste, agridulces, rojas, asadas).
comidas(tamales_duranguenses, durango, masa_maiz, comida, carnes, noreste, dulces, verdes, suaves).
comidas(empanadas_de_chilacayote, durango, chilacayote, postre, dulces, noreste, dulces, varia, suaves).
comidas(salsa_borracha, estado_mexico, chile_ancho, comida, verduras, centro, salsas, varia, liquido).
comidas(cinicuiles, estado_mexico, larva_polilla, comida, carnes, centro,carnes , rojos, suave).
comidas(atole_agrio, estado_mexico, maiz_negro, comida, verduras, centro, salsas, varia, liquido).
comidas(caldillo_duranguense, noreste, durango, sopa, res, salado, comida, alto).
comidas(enchiladas_duranguenses, noreste, durango, platofuerte, maiz, salado, comida, medio).
comidas(dulce_de_nuez, noreste, durango, postre, nuez, dulce, comida, medio).
comidas(gallinas_borrachas, noreste, durango, platofuerte, pollo, salado, comida, medio).
comidas(tamales_duranguenses, noreste, durango, platofuerte, maiz, salado, cena, alto).
comidas(empanadas_de_chilacayote, noreste, durango, postre, chilacayote, dulce, cena, alto).
comidas(salsa_borracha, centro, estado_de_mexico, condimento, chile, picante, comida, alto).
comidas(chinicuiles, centro, estado_de_mexico, entremes, insectos, salado, comida, medio).
comidas(elote_agrio, centro, estado_de_mexico, bebida, maiz, acido, comida, medio).
comidas(enchiladas_mineras, occidente, guanajuato, platofuerte, maiz, salado, comida, medio).
comidas(fiambre, occidente, guanajuato, platofuerte, res, salado, comida, alto).
comidas(cajeta, occidente, guanajuato, postre, nuez, dulce, comida, alto).
comidas(fresas_cristalizadas, occidente, guanajuato, postre, frutas, dulce, comida, alto).
comidas(gorditas, occidente, guanajuato, platofuerte, cerdo, salado, comida, alto).
comidas(pozole_guerrerense, centro, guerrero, sopa, cerdo, salado, comida, alto).
comidas(pescado_a_la_talla, centro, guerrero, platofuerte, pescado, salado, comida, medio).
comidas(tamales_por_region, centro, guerrero, pan, maiz, salado, comida, medio).
comidas(comidas_de_jumil, centro, guerrero, platofuerte, xomitl, salado, comida, medio).
comidas(pulpo_enamorado, centro, guerrero, platofuerte, pulpo, salado, comida, medio).
comidas(chilate, centro, guerrero, bebida, maiz, dulce, comida, medio).
comidas(elopozole, centro, guerrero, platofuerte, maiz, salado, comida, medio).
comidas(pastes, centro, hidalgo, platofuerte, lacteo, salado, comida, medio).
comidas(gusanos_de_maguey, centro, hidalgo, entremes, insectos, salado, comida, medio).
comidas(zacahuil, centro, hidalgo, entremes, chile, picante, comida, medio).
comidas(escamoles, centro, hidalgo, platofuerte, insectos, picante, comida, medio).
comidas(torta_ahogada, centro, jalisco, pan, cerdo, salado, comida, alto).
comidas(tejuino_jalisiense, centro, jalisco, bebida, maiz, agridulce, comida, alto).
comidas(jericalla, centro, jalisco, postre, huevo, dulce, comida, alto).
comidas(cazuelas, centro, jalisco, bebida, frutas, dulce, comida, medio).
comidas(olla_podrida, occidente, michoacan, platofuerte, res, salado, comida, medio).
comidas(uchepos, occidente, michoacan, pan, maiz, dulce, comida, medio).
comidas(gazpacho, occidente, michoacan, entremes, frutas, dulce, comida, medio).
comidas(atapakua, occidente, michoacan, entremes, chile, picante, comida, medio).
comidas(aporreado, occidente, michoacan, platofuerte, cerdo, salado, comida, alto).
comidas(chongoz_zamoranos, occidente, michoacan, postre, lacteo, dulce, cena, alto).
comidas(cecina, centro, morelos, platofuerte, res, salado, comida, medio).
comidas(pipian_verde, centro, morelos, platofuerte, pollo, salado, comida, medio).
comidas(caldo_de_hongos, centro, morelos, sopa, vegetales, salado, comida, medio).
comidas(tacos_acorazados, centro, morelos, entremes, res, salado, comida, alto).
comidas(pozole_blanco, centro, morelos, sopa, maiz, salado, comida, alto).
comidas(tamal_de_pescado, centro, morelos, pan, pescado, salado, comida, alto).
comidas(esquites, centro, morelos, entremes, maiz, agridulce, comida, alto).
comidas(huauzontle, centro, morelos, entremes, vegetales, salado, comida, alto).
comidas(tlacoyos_de_morelos, centro, morelos, platofuerte, maiz, salado, cena, alto).
comidas(itacates, centro, morelos, platofuerte, vegetales, salado, cena, alto).
comidas(tacos_de_chapulines_al_ajillo, centro, morelos, platofuerte, insectos, salado, comida, medio).
comidas(frijoles_ayocotes_en_adobo, centro, morelos, platofuerte, frijoles, salado, comida, medio).
comidas(pescado_sarandeado, occidente, nayarit, platofuerte, pescado, salado, comida, medio).
comidas(pollo_al_estilo_ixtlan_del_rio, occidente, nayarit, platofuerte, pollo, salado, comida, medio).
comidas(pipian_de_pepitas_de_calabaza, occidente, nayarit, platofuerte, pollo, picante, comida, medio).
comidas(frijoles_puercos_de_nayarit, occidente, nayarit, platofuerte, frijol, salado, comida, medio).
